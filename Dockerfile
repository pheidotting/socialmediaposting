FROM hypriot/rpi-java

COPY target/socialmediaposting.jar /home

EXPOSE 8080

ENTRYPOINT ["java","-jar","/home/socialmediaposting.jar"]
