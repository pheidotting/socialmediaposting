package nl.heidotting.socialmediaposting.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import nl.heidotting.socialmediaposting.domain.IngeplandePost;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Service
public class IngeplandePostRepository {
    private final static Logger LOGGER = LoggerFactory.getLogger(IngeplandePostRepository.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Session getSession() {
        Session session = null;
        if (entityManager == null || (session = entityManager.unwrap(Session.class)) == null) {

            throw new NullPointerException();
        }

        return session;
    }

    @Transactional
    public void opslaan(IngeplandePost ingeplandePost){
        LOGGER.debug("Opslaan IngeplandePost");

            if (ingeplandePost.getId() == null) {
                getSession().save(ingeplandePost);
            } else {
                getSession().merge(ingeplandePost);
            }
    }

    @Transactional
    public List<IngeplandePost> alles(){
        List<IngeplandePost> list =leesIngeplandePostsInDeToekomst();
        list.addAll(leesUitTeVoerenPosts());

        return list;
    }

    @Transactional
    public List<IngeplandePost> leesUitTeVoerenPosts(){
        Query<IngeplandePost> query=getSession().getNamedQuery("IngeplandePost.leesUitTeVoerenPosts");
        query.setParameter("nu",LocalDateTime.now());

        return query.getResultList();
    }

    @Transactional
    public List<IngeplandePost> leesIngeplandePostsInDeToekomst(){
        Query<IngeplandePost> query=getSession().getNamedQuery("IngeplandePost.ingeplandePostsInDeToekomst");
        query.setParameter("nu",LocalDateTime.now());

        return query.getResultList();
    }

    @Transactional
    public void verwijder(Long id){
        IngeplandePost ingeplandePost=getSession().find(IngeplandePost.class,id);

        getSession().delete(ingeplandePost);
    }
    @Transactional
    public IngeplandePost lees(Long id){
        return getSession().find(IngeplandePost.class,id);
    }
}
