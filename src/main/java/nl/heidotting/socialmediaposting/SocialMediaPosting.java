package nl.heidotting.socialmediaposting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SocialMediaPosting {
    public static void main(String[] args) {
        SpringApplication.run(SocialMediaPosting.class, args);
    }
}
