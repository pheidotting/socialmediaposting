package nl.heidotting.socialmediaposting.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;

public class Bestand {
    private String uuid;
    private Integer id;
    private File file;
    private Set<KentekenMetWaardeMetHashtag> kentekenMetWaardeMetHashtags = newHashSet();

    public Bestand() {
        this.uuid = UUID.randomUUID().toString();
    }

    public Bestand(File file, Set<KentekenMetWaardeMetHashtag> kentekenMetWaardeMetHashtags) {
        this();
        this.file = file;
        this.kentekenMetWaardeMetHashtags = kentekenMetWaardeMetHashtags;
    }

    public Bestand(File file) {
        this();
        this.file = file;
    }

    public Bestand(File file, List<String> kentekens, boolean dummy) {
        this();
        this.file = file;
        setKentekens(kentekens);
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setKentekens(List<String> kentekens) {
        this.kentekenMetWaardeMetHashtags.addAll(kentekens.stream().map(kenteken -> new KentekenMetWaardeMetHashtag(kenteken)).collect(Collectors.toList()));
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Set<KentekenMetWaardeMetHashtag> getKentekenMetWaardeMetHashtags() {
        return kentekenMetWaardeMetHashtags;
    }

    public void setKentekenMetWaardeMetHashtags(Set<KentekenMetWaardeMetHashtag> kentekenMetWaardeMetHashtags) {
        this.kentekenMetWaardeMetHashtags = kentekenMetWaardeMetHashtags;
    }

    public String getTekst() {
        StringBuilder tekst = new StringBuilder();
        Set<String> hashtags = new HashSet<>();
        List<String> hashtagsList;
        StringBuilder totaal = new StringBuilder();

        kentekenMetWaardeMetHashtags.stream().forEach(kentekenMetWaardeMetHashtag -> {
            kentekenMetWaardeMetHashtag.getWaardeMetHashtags().stream().forEach(waardeMetHashtag -> {
                tekst.append(waardeMetHashtag.getWaarde());
                tekst.append(" ");

                if (waardeMetHashtag.getHashtag() != null) {
                    hashtags.add(waardeMetHashtag.getHashtag());
                }
            });
            kentekenMetWaardeMetHashtag.getHashtags().stream().forEach(hashtag -> hashtags.add(hashtag));
            tekst.append("\n");
        });

        if (!"".equals(tekst.toString().trim())) {
            totaal.append(tekst);
            totaal.append("\n\n");
        }
        hashtagsList = newArrayList(hashtags);
        Collections.shuffle(hashtagsList);
        totaal.append(hashtagsList.stream().reduce("", (partialString, element) -> partialString + "#" + element + " "));
        return totaal.toString();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(null).append("file", file == null ? "null" : file.getName()).append("kentekenMetWaardeMetHashtags", kentekenMetWaardeMetHashtags).toString();
    }
}
