package nl.heidotting.socialmediaposting.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "INGEPLANDEPOSTS")
@NamedQueries({//
         @NamedQuery(name = "IngeplandePost.ingeplandePostsInDeToekomst", query = "select i from IngeplandePost i where i.tijdstipIngepland > :nu"),//
         @NamedQuery(name = "IngeplandePost.leesUitTeVoerenPosts", query = "select i from IngeplandePost i where i.tijdstipIngepland < :nu"),//
         @NamedQuery(name = "IngeplandePost.alles", query = "select i from IngeplandePost i")//
         })
public class IngeplandePost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "INGEPLAND")
    private LocalDateTime tijdstipIngepland;
    @Column(name = "BESTANDSNAAM")
    private String bestandsnaam;
    @Column(name = "MEDIA")
    private Media media;
    @Column(name = "TEKST")
    private String tekst;

    public IngeplandePost() {
    }

    public IngeplandePost(LocalDateTime tijdstipIngepland, String bestandsnaam, Media media, String tekst) {
        this.tijdstipIngepland = tijdstipIngepland;
        this.bestandsnaam = bestandsnaam;
        this.media = media;
        this.tekst = tekst;
    }

    public IngeplandePost(String bestandsnaam, Media media, String tekst) {
        this.bestandsnaam = bestandsnaam;
        this.media = media;
        this.tekst = tekst;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTijdstipIngepland() {
        return tijdstipIngepland;
    }

    public void setTijdstipIngepland(LocalDateTime tijdstipIngepland) {
        this.tijdstipIngepland = tijdstipIngepland;
    }

    public String getBestandsnaam() {
        return bestandsnaam;
    }

    public void setBestandsnaam(String bestandsnaam) {
        this.bestandsnaam = bestandsnaam;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public IngeplandePost clone() {
        return new IngeplandePost( this.getBestandsnaam(), this.getMedia(), this.getTekst());
    }
}
