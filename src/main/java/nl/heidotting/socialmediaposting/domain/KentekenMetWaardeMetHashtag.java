package nl.heidotting.socialmediaposting.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class KentekenMetWaardeMetHashtag {
    private String kenteken;
    private List<WaardeMetHashtag> waardeMetHashtags = newArrayList();
    private List<String> hashtags = newArrayList();

    public KentekenMetWaardeMetHashtag() {
    }

    public KentekenMetWaardeMetHashtag(String kenteken) {
        this.kenteken = kenteken;
    }

    public KentekenMetWaardeMetHashtag(String kenteken, List<WaardeMetHashtag> waardeMetHashtags) {
        this.kenteken = kenteken;
        this.waardeMetHashtags = waardeMetHashtags;
    }

    public String getKenteken() {
        return kenteken;
    }

    public void setKenteken(String kenteken) {
        this.kenteken = kenteken;
    }

    public List<WaardeMetHashtag> getWaardeMetHashtags() {
        return waardeMetHashtags;
    }

    public void setWaardeMetHashtags(List<WaardeMetHashtag> waardeMetHashtags) {
        this.waardeMetHashtags = waardeMetHashtags;
    }

    public List<String> getHashtags() {
        return hashtags;
    }

    public void setHashtags(List<String> hashtags) {
        this.hashtags = hashtags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof KentekenMetWaardeMetHashtag)) {
            return false;
        }

        KentekenMetWaardeMetHashtag that = (KentekenMetWaardeMetHashtag) o;

        return new EqualsBuilder().append(getKenteken(), that.getKenteken()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getKenteken()).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("kenteken", kenteken).append("waardeMetHashtags", waardeMetHashtags).append("hashtags", hashtags).toString();
    }
}
