package nl.heidotting.socialmediaposting.domain;

public enum Media {
    FACEBOOK, INSTAGRAM;
}
