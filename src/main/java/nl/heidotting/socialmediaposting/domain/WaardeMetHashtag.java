package nl.heidotting.socialmediaposting.domain;


import org.apache.commons.lang3.builder.ToStringBuilder;

public class WaardeMetHashtag {
    private String waarde;
    private String hashtag;

    public WaardeMetHashtag() {
    }

    public WaardeMetHashtag(String waarde, String hashtag) {
        this.waarde = waarde;
        this.hashtag = hashtag;
    }

    public String getWaarde() {
        return waarde;
    }

    public void setWaarde(String waarde) {
        this.waarde = waarde;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("waarde", waarde).append("hashtag", hashtag).toString();
    }
}
