package nl.heidotting.socialmediaposting.service;

import nl.heidotting.socialmediaposting.domain.IngeplandePost;
import nl.heidotting.socialmediaposting.repository.IngeplandePostRepository;
import nl.heidotting.socialmediaposting.service.socialmedia.SocialMediaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Service
public class PlaatsMediaOpSocialMediaService {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlaatsMediaOpSocialMediaService.class);

    @Inject
    private List<SocialMediaService> socialMediaServices;

    @Inject
    private IngeplandePostRepository ingeplandePostRepository;
    @Inject
    private InplannenSocialMediaBerichtenService inplannenSocialMediaBerichtenService;

    public void plaatsen() {
        List<IngeplandePost> ingeplandePosts = ingeplandePostRepository.leesUitTeVoerenPosts();
        if (ingeplandePosts.size() > 0) {
            if (doorgaan()) {

                LOGGER.debug("ingeplandePosts {}", ingeplandePosts.size());
                LocalDateTime eenUurGeleden = LocalDateTime.now().minusHours(1);
                if (ingeplandePosts.size() > 1 || (ingeplandePosts.size() > 0 && ingeplandePosts.get(0).getTijdstipIngepland().isBefore(eenUurGeleden))) {
                    LOGGER.debug("Meer dan één post, herplannen");
                    inplannenSocialMediaBerichtenService.startInplannen(true);
                } else {
                    LOGGER.debug("Uitvoeren");
                    //Lijst bevat maar 1, stream() of get(0)
                    ingeplandePosts.stream().forEach(ingeplandePost -> {
                        SocialMediaService socialMediaService = socialMediaServices.stream()//
                                .filter(socialMediaService1 -> socialMediaService1.voorMij(ingeplandePost.getMedia()))//
                                .findFirst()//
                                .orElse(null);

                        try {
                            socialMediaService.uitvoeren(ingeplandePost);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
                }
            } else {
                LOGGER.debug("Mag nu niets uitvoeren, maar er is nog een (of meer) post(s) die uitgevoerd moeten worden, herplannen");
                inplannenSocialMediaBerichtenService.startInplannen(true);
            }
        }
    }

    private boolean doorgaan() {
        boolean doorgaan = true;

        LocalTime nu = LocalTime.now();

        PlanTijdstipService.Dag dag = PlanTijdstipService.Dag.getFromDayOfWeek();

        if (dag.getMaximumAantalPosts() > 0) {
            doorgaan = false;
        }

        if (dag.getStartTijd().isAfter(nu) || dag.getEindTijd().isBefore(nu)) {
            doorgaan = false;
        }

        return doorgaan;
    }
}
