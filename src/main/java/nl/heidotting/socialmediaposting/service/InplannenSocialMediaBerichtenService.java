package nl.heidotting.socialmediaposting.service;

import com.google.common.util.concurrent.RateLimiter;
import nl.heidotting.socialmediaposting.domain.*;
import nl.heidotting.socialmediaposting.repository.IngeplandePostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.File;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static nl.heidotting.socialmediaposting.service.StackStorageService.StackStoragePaden.PATH_IN_TE_PLANNEN_BESTANDEN;

@Service
public class InplannenSocialMediaBerichtenService {
    private final static Logger LOGGER = LoggerFactory.getLogger(InplannenSocialMediaBerichtenService.class);

    @Inject
    private StackStorageService stackStorageService;
    @Inject
    private PlateRecognitionService plateRecognitionService;
    @Inject
    private PlanTijdstipService planTijdstipService;
    @Inject
    private RdwService rdwService;
    @Inject
    private IngeplandePostRepository ingeplandePostRepository;

    private RateLimiter rateLimiter = RateLimiter.create(1);

    public int startInplannen() {
        return startInplannen(false);
    }

    public int startInplannen(boolean force) {
        LOGGER.info("Start Inplannen");
        int aantal = 0;
        List<File> list = stackStorageService.lijstMetBestandenEnMappen(PATH_IN_TE_PLANNEN_BESTANDEN);
        List<File> lijstInTePlannen = newArrayList();
        if (list.size() > 49) {
            lijstInTePlannen.addAll(list.subList(0, 50));
        } else {
            lijstInTePlannen.addAll(list);
        }
        aantal = aantal + lijstInTePlannen.size();

        while (!lijstInTePlannen.isEmpty()) {
            LOGGER.info("Inplannen {} nieuwe bestanden", lijstInTePlannen.size());
            start(lijstInTePlannen, force);

            lijstInTePlannen = stackStorageService.lijstMetBestandenEnMappen(PATH_IN_TE_PLANNEN_BESTANDEN).subList(0, 50);
            aantal = aantal + lijstInTePlannen.size();
        }

        return aantal;
    }

    public int start(List<File> lijstInTePlannen, boolean force) {
        if (lijstInTePlannen.size() > 0 || force) {
            lijstInTePlannen.stream().forEach(file -> LOGGER.debug(file.getPath()));
            List<Bestand> lijst = lijstInTePlannen.stream()//
                    .filter(davResource -> !davResource.isDirectory())//
                    .map(stackStorageService.asBestand())//
                    .map(bestand -> {
                        String replace = PATH_IN_TE_PLANNEN_BESTANDEN.getPad() + "/";
                        String[] pad = bestand.getFile().getPath().replace(replace, "").split("/");

                        newArrayList(pad).stream().forEach(s -> {
                            if (!s.toLowerCase().endsWith(".jpg")) {
                                if (isNumeric(s)) {
                                    bestand.setId(Integer.parseInt(s));
                                } else {
                                    KentekenMetWaardeMetHashtag kentekenMetWaardeMetHashtag = new KentekenMetWaardeMetHashtag();
                                    kentekenMetWaardeMetHashtag.getHashtags().addAll(InplannenSocialMediaBerichtenService.this.maakTagsUitDirNaam(s));

                                    bestand.getKentekenMetWaardeMetHashtags().add(kentekenMetWaardeMetHashtag);
                                }
                            }
                        });

                        return bestand;
                    }).collect(Collectors.toList());

            if (lijst.size() > plateRecognitionService.getCallsNogOver()) {
                LOGGER.info("Geen plate recognition calls meer over");
                return 0;
            }

            lijst.stream()//
                    .map(plateRecognitionService.recognize(rateLimiter))//
                    .map(rdwService.leesHashTagsMetWaade(rateLimiter))//
                    .forEach(voegYoungtimerOldtimerEnKYVTagsToe());//

            Map<Integer, Set<KentekenMetWaardeMetHashtag>> kentekenMetWaardeMetHashtag = new HashMap<>();
            lijst.stream()//
                    .filter(bestand -> bestand.getId() != null)//
                    .forEach(bestand -> {
                        Set<KentekenMetWaardeMetHashtag> k = kentekenMetWaardeMetHashtag.get(bestand.getId());
                        if (k == null) {
                            k = new HashSet<>();
                        }
                        k.addAll(bestand.getKentekenMetWaardeMetHashtags());
                        kentekenMetWaardeMetHashtag.put(bestand.getId(), k);
                    });

            lijst.stream()//
                    .filter(bestand -> bestand.getId() != null)//
                    .forEach(bestand -> bestand.setKentekenMetWaardeMetHashtags(kentekenMetWaardeMetHashtag.get(bestand.getId())));

            List<IngeplandePost> ingeplandePosts = ingeplandePostRepository.alles().stream()//
                    .map(ingeplandePost -> {
                        IngeplandePost clone = ingeplandePost.clone();

                        ingeplandePostRepository.verwijder(ingeplandePost.getId());

                        return clone;
                    })//
                    .collect(Collectors.toList());// ;
            lijst.stream().forEach(maakIngeplandePosts(ingeplandePosts));

            Collections.shuffle(ingeplandePosts);
            ingeplandePosts.stream().forEach(ingeplandePost -> {
                ingeplandePost.setTijdstipIngepland(planTijdstipService.komMetTijdstip());

                ingeplandePostRepository.opslaan(ingeplandePost);
            });

            ingeplandePosts.stream().forEach(ingeplandePost -> LOGGER.debug("{} - {} - {}", ingeplandePost.getTijdstipIngepland(), ingeplandePost.getMedia(), ingeplandePost.getTekst().replace("\\n", "")));

            lijstInTePlannen.stream().forEach(stackStorageService.verwijderFunction());

            LOGGER.info("{} nieuwe posts ingepland", ingeplandePosts.size());

            return ingeplandePosts.size();
        } else {
            return 0;
        }
    }

    private List<String> maakTagsUitDirNaam(String dirNaam) {
        return newArrayList(dirNaam.split("-"));
    }

    private Consumer<Bestand> maakIngeplandePosts(List<IngeplandePost> ingeplandePosts) {
        return bestand -> {
            for (Media media : Media.values()) {
                String nieuweNaam = stackStorageService.kopieerNaarIngepland(bestand, media);
                IngeplandePost ingeplandePost = new IngeplandePost(nieuweNaam, media, bestand.getTekst());
                ingeplandePosts.add(ingeplandePost);
            }

            //verwijder bestand
            stackStorageService.verwijder(bestand.getFile());
        };
    }

    private Consumer<Bestand> voegYoungtimerOldtimerEnKYVTagsToe() {
        return bestand -> bestand.getKentekenMetWaardeMetHashtags().stream().forEach(kentekenMetWaardeMetHashtag -> {
            WaardeMetHashtag bouwjaarTag = kentekenMetWaardeMetHashtag.getWaardeMetHashtags().stream().filter(waardeMetHashtag -> {
                boolean isBouwjaar;
                try {
                    Integer.parseInt(waardeMetHashtag.getWaarde());
                    isBouwjaar = true;
                } catch (NumberFormatException nfe) {
                    isBouwjaar = false;
                }
                return waardeMetHashtag.getWaarde() != null && waardeMetHashtag.getHashtag() == null && isBouwjaar;
            }).findFirst().orElse(null);
            if (bouwjaarTag != null) {
                int bouwjaar = Integer.parseInt(bouwjaarTag.getWaarde());

                int leeftijd = LocalDate.now().getYear() - bouwjaar;
                if (leeftijd > 15 && leeftijd < 40) {
                    kentekenMetWaardeMetHashtag.getHashtags().add("youngtimer");
                } else if (leeftijd > 40) {
                    kentekenMetWaardeMetHashtag.getHashtags().add("oldtimer");
                }
            }
            kentekenMetWaardeMetHashtag.getHashtags().add("klazienaveneryoungtimervrienden");
        });
    }

    private boolean isNumeric(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
