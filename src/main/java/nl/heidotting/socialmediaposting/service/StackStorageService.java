package nl.heidotting.socialmediaposting.service;

import nl.heidotting.socialmediaposting.domain.Bestand;
import nl.heidotting.socialmediaposting.domain.Media;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static nl.heidotting.socialmediaposting.service.StackStorageService.StackStoragePaden.PATH_INPLANDE_BESTANDEN;
import static nl.heidotting.socialmediaposting.service.StackStorageService.StackStoragePaden.PATH_IN_TE_PLANNEN_BESTANDEN;

@Service
public class StackStorageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StackStorageService.class);

    public enum StackStoragePaden {
        PATH_IN_TE_PLANNEN_BESTANDEN("/home/pi/kyv/InTePlannen"), PATH_INPLANDE_BESTANDEN("/home/pi/kyv/InGeplandeBestanden");

        private String pad;

        StackStoragePaden(String pad) {
            this.pad = pad;
        }

        public String getPad() {
            return pad;
        }
    }
    //    private Sardine sardine;

    //    @PostConstruct
    //    public void init() {
    //        sardine = SardineFactory.begin("pheidotting", "Herman79!");
    //    }
    //
    //    private final String WEBDAV_SERVER = "https://pheidotting.stackstorage.com";
    //    private final String WEBDAV_PATH = "/remote.php/webdav/KYV";
    //
    //    public String getWEBDAV_PATH() {
    //        return WEBDAV_PATH;
    //    }

    private File getFile(String bestandsnaam) {
        return new File(bestandsnaam);
    }

    public void verwijder(String bestandsnaam, Media media) {
        String bestand = PATH_INPLANDE_BESTANDEN.getPad() + "/" + media.name() + "-" + bestandsnaam + ".jpg";
        LOGGER.debug("Verwijderen {}", bestand);
        getFile(bestand).delete();

        String bestandJson = PATH_INPLANDE_BESTANDEN.getPad() + "/" + bestandsnaam + ".json";
        LOGGER.debug("Verwijderen {}", bestandJson);
        getFile(bestandJson).delete();
    }

    public Consumer<File> verwijderFunction() {
        return new Consumer<File>() {
            @Override
            public void accept(File davResource) {
                verwijder(davResource);
            }
        };
    }

    public void verwijder(File file) {
        if (file.exists()) {
            file.delete();
            lijstMetBestandenEnMappen(PATH_IN_TE_PLANNEN_BESTANDEN).stream()//
                    .filter(file1 -> file1.isDirectory())//
                    .filter(map -> lijstMetBestandenEnMappen(map).size() > 0)//
                    .filter(map -> !map.toString().endsWith(PATH_IN_TE_PLANNEN_BESTANDEN.getPad()) && !map.toString().endsWith(PATH_IN_TE_PLANNEN_BESTANDEN.getPad() + "/"))//
                    .forEach(map -> {
                        if (lijstMetBestandenEnMappen(map).size() == 1) {
                            LOGGER.debug("DELETE MAP {}", map.toString());
                            map.delete();
                        }
                    });
        }
    }


    //    public void copy(File davResource, String newName) {
    //        String slash = "";
    //        if (!newName.startsWith("/")) {
    //            slash = "/";
    //        }
    //
    //        String in = davResource.toString();
    //        String uit = slash + newName;
    //
    //        try {
    //            LOGGER.debug("Kopieren {}", in);
    //            LOGGER.debug("naar     {}", uit);
    //            FileCopyUtils.copy(davResource, new File(newName));
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        }
    //    }

    public List<File> lijstMetBestandenEnMappen(StackStoragePaden pad) {
        return lijstMetBestandenEnMappen(getFile(pad.getPad()));
    }

    public List<File> lijstMetBestandenEnMappen(File padIn) {
        final List<File> list = newArrayList();
        if (padIn.exists()) {
            list.addAll(newArrayList(padIn.listFiles()));
        }

        newArrayList(list).stream()//
                .filter(file -> file.isDirectory())//
                .forEach(directory -> list.addAll(lijstMetBestandenEnMappen(directory)));

        return list.stream()//
                .filter(file -> !file.getName().equals(".DS_Store"))//
                .filter(file -> !file.isDirectory())//
                .collect(Collectors.toList());
    }

    public List<File> inlezenIngeplandeBestanden() {
        File[] list = getFile(PATH_INPLANDE_BESTANDEN.getPad()).listFiles();
        return list == null ? newArrayList() : newArrayList(list);
    }

    public String kopieerNaarIngepland(Bestand bestand, Media media) {
        String newName = UUID.randomUUID().toString();

        try {
            FileCopyUtils.copy(bestand.getFile(), getFile(PATH_INPLANDE_BESTANDEN.getPad() + "/" + media.name() + "-" + newName + ".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return newName;
    }

    public String leesJson(File input) {
        String in = input.getPath();
        LOGGER.debug("leesJson uit {}", input);

        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(in))) {


            String line;
            while ((line = br.readLine()) != null) {

                sb.append(line);
                sb.append(System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOGGER.debug("Gelezen {}", sb);
        return sb.toString();
    }


    public InputStream getIngeplandBestand(String naam) {
        String bestandsnaam = PATH_INPLANDE_BESTANDEN.getPad() + "/" + naam;
        LOGGER.info("Ophalen {}", bestandsnaam);
        try {
            return new FileInputStream(getFile(bestandsnaam));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Function<File, Bestand> asBestand() {
        return file -> {
            try {
                return new Bestand(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        };
    }

}
