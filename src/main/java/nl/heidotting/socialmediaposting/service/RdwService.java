package nl.heidotting.socialmediaposting.service;

import com.google.common.util.concurrent.RateLimiter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import nl.heidotting.socialmediaposting.domain.Bestand;
import nl.heidotting.socialmediaposting.domain.WaardeMetHashtag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class RdwService {
    private final static Logger LOGGER = LoggerFactory.getLogger(RdwService.class);

    public Function<Bestand, Bestand> leesHashTagsMetWaade(RateLimiter rateLimiter) {
        return bestand -> {
            bestand.getKentekenMetWaardeMetHashtags().stream()//
                    .filter(kentekenMetWaardeMetHashtag -> kentekenMetWaardeMetHashtag.getKenteken()!=null&&!"".equals(kentekenMetWaardeMetHashtag.getKenteken()))
             .forEach(kentekenMetWaardeMetHashtag -> {
                String kenteken = kentekenMetWaardeMetHashtag.getKenteken().replace("-", "").toUpperCase();
                LOGGER.debug("Ophalen gegevens voor kenteken {}", kenteken);

                ClientConfig clientConfig = new DefaultClientConfig();
                clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
                Client client = Client.create(clientConfig);
                 WebResource webResource = client.resource("http://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=" + kenteken);
                rateLimiter.acquire();
                ClientResponse response = webResource.header("X-App-Token", "9HYD14uUG3kN5aobEr3phPHJe").accept("application/json").type("application/json").get(ClientResponse.class);

                String res = response.getEntity(String.class);
                LOGGER.trace("Response: {}",res);

                int posMerk = res.indexOf("\"merk\"");
                if (posMerk > 1) {
                    int posMerkEnd = res.indexOf("\"", posMerk + 8);
                    String merk = res.substring(posMerk + 8, posMerkEnd);
                    int posHandelsbenaming = res.indexOf("\"handelsbenaming\"");
                    int posHandelsbenamingEnd = res.indexOf("\"", posHandelsbenaming + 19);
                    String handelsbenaming = res.substring(posHandelsbenaming + 19, posHandelsbenamingEnd);
                    int posDatumEersteToelating = res.indexOf("\"datum_eerste_toelating\"");
                    String bouwjaar = res.substring(posDatumEersteToelating + 26, posDatumEersteToelating + 30);

                    LOGGER.debug("Merk : {}, Handelsbenaming : {}, Bouwjaar : {}", merk,handelsbenaming,bouwjaar);

                    handelsbenaming = handelsbenaming.replace(" ", "").replace(";", "").replace(".", "");

                    kentekenMetWaardeMetHashtag.getWaardeMetHashtags().add(new WaardeMetHashtag(merk, verwerkHashtag(merk)));
                    kentekenMetWaardeMetHashtag.getWaardeMetHashtags().add(new WaardeMetHashtag(handelsbenaming, verwerkHashtag(handelsbenaming)));
                    kentekenMetWaardeMetHashtag.getWaardeMetHashtags().add(new WaardeMetHashtag(bouwjaar, verwerkHashtag(bouwjaar)));
                }
            });

            return bestand;
        };
    }

    private String verwerkHashtag(String hash) {
        String hashtag = hash.replace(" ", "").replace("-", "").replace("*", "").replace(",", "").replace("/", "");

        try {
            Integer.parseInt(hashtag);

            return null;
        } catch (Exception e) {
            return hashtag.toLowerCase();
        }
    }
}
