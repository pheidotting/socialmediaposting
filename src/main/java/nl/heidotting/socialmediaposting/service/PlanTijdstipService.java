package nl.heidotting.socialmediaposting.service;

import nl.heidotting.socialmediaposting.domain.IngeplandePost;
import nl.heidotting.socialmediaposting.repository.IngeplandePostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.time.DayOfWeek.*;

@Service
public class PlanTijdstipService {
    private final static Logger LOGGER = LoggerFactory.getLogger(PlanTijdstipService.class);

    @Inject
    private IngeplandePostRepository ingeplandePostRepository;

private     Map<LocalDate, Integer> mapMetAantalPosts = new HashMap<>();

    public enum Dag {
//        Maandag(MONDAY, 0, 0, 17, 0, 22, 30), //
//        Dinsdag(TUESDAY, 0, 0, 17, 0, 22, 30), //
//        Woensdag(WEDNESDAY, 4, 6, 17, 0, 22, 30), //
//        Donderdag(THURSDAY, 4, 6, 17, 0, 22, 30), //
//        Vrijdag(FRIDAY, 4, 6, 17, 0, 22, 30), //
//        Zaterdag(SATURDAY, 6, 8, 15, 0, 22, 30), //
//        Zondag(SUNDAY, 8, 12, 12, 0, 22, 30);
        Maandag(MONDAY, 0, 0, 17, 0, 22, 30), //
        Dinsdag(TUESDAY, 0, 0, 17, 0, 22, 30), //
        Woensdag(WEDNESDAY, 0, 0, 17, 0, 22, 30), //
        Donderdag(THURSDAY, 1, 1, 17, 0, 22, 30), //
        Vrijdag(FRIDAY, 1, 2, 17, 0, 22, 30), //
        Zaterdag(SATURDAY, 2, 3, 15, 0, 22, 30), //
        Zondag(SUNDAY, 2, 4, 12, 0, 22, 30);

        private DayOfWeek dayOfWeek;
        private int minimumAantalPosts;
        private int maximumAantalPosts;
        private LocalTime startTijd;
        private LocalTime eindTijd;

        Dag(DayOfWeek dayOfWeek, int minimumAantalPosts, int maximumAantalPosts, int startTijdUur, int startTijdMinuut, int eindTijdUur, int eindTijdMinuut) {
            this.dayOfWeek = dayOfWeek;
            this.minimumAantalPosts = minimumAantalPosts;
            this.maximumAantalPosts = maximumAantalPosts;
            this.startTijd = LocalTime.of(startTijdUur, startTijdMinuut);
            this.eindTijd = LocalTime.of(eindTijdUur, eindTijdMinuut);
        }

        public DayOfWeek getDayOfWeek() {
            return dayOfWeek;
        }

        public int getMinimumAantalPosts() {
            return minimumAantalPosts;
        }

        public int getMaximumAantalPosts() {
            return maximumAantalPosts;
        }

        public LocalTime getStartTijd() {
            return startTijd;
        }

        public LocalTime getEindTijd() {
            return eindTijd;
        }

        public static Dag getFromDayOfWeek() {
            return getFromDayOfWeek(LocalDate.now().getDayOfWeek());
        }

        public static Dag getFromDayOfWeek(DayOfWeek dayOfWeek) {
            for (Dag dag : Dag.values()) {
                if (dag.getDayOfWeek() == dayOfWeek) {
                    return dag;
                }
            }
            return null;
        }

    }

    public LocalDateTime komMetTijdstip() {
        return komMetTijdstip(1, 1);
    }

    private LocalDateTime komMetTijdstip(int recursive, int hoeveelstekeer) {
        if (hoeveelstekeer > 10) {
            return komMetTijdstip(recursive + 1, 1);
        }
        List<IngeplandePost> ingeplandePosts = ingeplandePostRepository.leesIngeplandePostsInDeToekomst();

        int dagenNaNu = ThreadLocalRandom.current().nextInt(0, 50 * recursive);

        LocalDate datum = LocalDate.now().plusDays(dagenNaNu);

        Dag dag = Dag.getFromDayOfWeek(datum.getDayOfWeek());

        int startTijdUur = dag.getStartTijd().getHour();
        if (datum.equals(LocalDate.now())) {
            startTijdUur = LocalTime.now().getHour() + 1;
        }

        int uur;
        try {
            uur = ThreadLocalRandom.current().nextInt(startTijdUur, dag.getEindTijd().getHour());
        } catch (IllegalArgumentException e) {
            return komMetTijdstip(recursive, ++hoeveelstekeer);
        }
        int minuut = ThreadLocalRandom.current().nextInt(dag.getStartTijd().getMinute(), dag.getEindTijd().getMinute());

        LocalTime tijd = LocalTime.of(uur, minuut);

        LocalDateTime tijdstip = LocalDateTime.of(datum, tijd);

        Integer aantalPosts = mapMetAantalPosts.get(datum);
        if (aantalPosts == null || aantalPosts == 0) {
            aantalPosts = ThreadLocalRandom.current().nextInt(dag.getMinimumAantalPosts() * 100, (dag.getMaximumAantalPosts() + 1) * 100) / 100;
            mapMetAantalPosts.put(datum, aantalPosts);
        }

        if (aantalPosts > dag.getMaximumAantalPosts()) {
            aantalPosts = dag.getMaximumAantalPosts();
        }

        int tussenRuimte = bepaalRuimteTussenPosts(dag.getStartTijd(), dag.getEindTijd(), aantalPosts);
        boolean nietTeDichtBijAnderen = nietTeDichtBijAnderen(ingeplandePosts.stream()//
                .filter(ingeplandePost -> ingeplandePost.getTijdstipIngepland().toLocalDate().equals(datum))//
                .map(ingeplandePost -> ingeplandePost.getTijdstipIngepland().toLocalTime()).collect(Collectors.toList()), tijd, tussenRuimte);
        if (!nietTeDichtBijAnderen) {
            return komMetTijdstip(recursive, ++hoeveelstekeer);
        }

        if (ingeplandePosts.stream().filter(ingeplandePost -> ingeplandePost.getTijdstipIngepland().toLocalDate().equals(datum)).collect(Collectors.toList()).size() + 1 > aantalPosts) {
            return komMetTijdstip(recursive, ++hoeveelstekeer);
        }

        if (ingeplandePosts.stream().anyMatch(ingeplandePost -> ingeplandePost.getTijdstipIngepland().equals(tijdstip))) {
            if (hoeveelstekeer < 11) {
                return komMetTijdstip(recursive, ++hoeveelstekeer);
            } else {
                return komMetTijdstip(recursive + 1, 1);
            }
        }

        //        IngeplandePost ingeplandePost = new IngeplandePost(tijdstip);
        //        ingeplandePostRepository.opslaan(ingeplandePost);

        return tijdstip;
        //        final List<LocalTime> result = new ArrayList<>();

        //        Dag dag = Dag.getFromDayOfWeek(datum.getDayOfWeek());
        //
        //        int aantalPosts = ThreadLocalRandom.current().nextInt(dag.getMinimumAantalPosts() * 100, (dag.getMaximumAantalPosts() + 1) * 100) / 100;
        //
        //
        //        if (aantalPosts > dag.getMaximumAantalPosts()) {
        //            aantalPosts = dag.getMaximumAantalPosts();
        //        }
        //
        //        LOGGER.info("Het is vandaag {}, dus we gaan {} posts inplannen per Social Media", dag.toString(), aantalPosts);
        //
        //        //        List<Bestand> bestanden = bestandService.leesRandom(aantalPosts);
        //
        //        //        LOGGER.info("{} Stack bestanden opgehaald", bestanden.size());
        //
        //        int tussenRuimte = bepaalRuimteTussenPosts(dag.getStartTijd(), dag.getEindTijd(), aantalPosts + ingeplandePosts.size());
        //
        //        final int[] teller = {0};
        //        for (int i = 0; i < aantalPosts; i++) {
        //
        //            //        }
        //            //        bestanden.stream().forEach(bestand -> {
        //            Random generator = new Random(System.currentTimeMillis());
        //            LocalTime randomTime = null;
        //            boolean ok = false;
        //
        //            while (!ok) {
        //                randomTime = LocalTime.MIN.plusSeconds(generator.nextLong());
        //                while (!pastBinnenTijdvakDag(dag, randomTime)) {
        //                    randomTime = LocalTime.MIN.plusSeconds(generator.nextLong());
        //                }
        //                boolean nietTeDichtBijAnderen = nietTeDichtBijAnderen(result, randomTime, tussenRuimte);
        //
        //                if (nietTeDichtBijAnderen) {
        //                    ok = true;
        //                } else {
        //                    teller[0]++;
        //                }
        //
        //                if (teller[0] > 20) {
        //                    result.clear();
        //                    return null;
        //                }
        //            }
        //            teller[0] = 0;
        //
        //            result.add(randomTime);
        //        }
        //
        //        if (teller[0] > 10) {
        //            return planPosts(datum, aantalAlAanwezig);
        //        }
        //
        //        return result;
    }

    private boolean pastBinnenTijdvakDag(Dag dag, LocalTime tijd) {
        if (LocalDateTime.of(LocalDate.now(), tijd).isBefore(LocalDateTime.of(LocalDate.now(), dag.getStartTijd()))) {
            return false;
        } else if (LocalDateTime.of(LocalDate.now(), tijd).isAfter(LocalDateTime.of(LocalDate.now(), dag.getEindTijd()))) {
            return false;
        }

        return true;
    }

    protected boolean nietTeDichtBijAnderen(List<LocalTime> geplandePosts, LocalTime localTime, int minutenTussenPosts) {
        final boolean[] ok = {true};
        geplandePosts.stream().sorted(new Comparator<LocalTime>() {
            @Override
            public int compare(LocalTime o1, LocalTime o2) {
                return o1.compareTo(o2);
            }
        }).forEach(new Consumer<LocalTime>() {
            @Override
            public void accept(LocalTime localTimeBestaand) {
                if (localTime.isAfter(localTimeBestaand.minusMinutes(minutenTussenPosts)) && localTime.isBefore(localTimeBestaand.plusMinutes(minutenTussenPosts))) {
                    ok[0] = false;
                }
            }
        });

        return ok[0];
    }

    protected int bepaalRuimteTussenPosts(LocalTime begintijd, LocalTime eindtijd, int aantalPosts) {
        long aantalMinuten = begintijd.until(eindtijd, ChronoUnit.MINUTES);

        long tussenruimte = aantalMinuten / (aantalPosts + 1);

        int result = (int) (tussenruimte - (tussenruimte * 0.5));

        if (result < 1) {
            result = 1;
        }
        return result;
    }

}
