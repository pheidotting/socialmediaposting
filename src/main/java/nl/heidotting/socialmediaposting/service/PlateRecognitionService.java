package nl.heidotting.socialmediaposting.service;

import com.google.common.util.concurrent.RateLimiter;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import nl.heidotting.socialmediaposting.domain.Bestand;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.function.Function;

import static com.google.common.collect.Lists.newArrayList;
@Service

public class PlateRecognitionService {
    private String token = "d301c4715074e4d7a57cb6126e44f2119aec6e3f";
    private boolean recursive = false;

    public Function<Bestand,Bestand> recognize(RateLimiter rateLimiter) {
        return file -> {
            rateLimiter.acquire();

            return voeruit(file, rateLimiter);
        };
    }

    private Bestand voeruit(Bestand bestand, RateLimiter rateLimiter) {
        File fileInput = bestand.getFile();
        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(fileInput);
        } catch (IOException e) {
            e.printStackTrace();
            return bestand;
        }
        int width          = bimg.getWidth();
        int height         = bimg.getHeight();

        File file;
        if(getFileSizeMegaBytes(fileInput)>3.0) {
            file=fileInput;
            while (getFileSizeMegaBytes(file) > 3.0) {
//                System.out.println(getFileSizeMegaBytes(file));
                try {
                    file = File.createTempFile("instagram", "post");
//                    file = new File("/Users/patrickheidotting/Documents/test/small-"+UUID.randomUUID().toString()+".jpg");
                    file.deleteOnExit();
                    try (FileOutputStream fos = new FileOutputStream(file.getPath())) {
//                        System.out.println(width);
//                        System.out.println(height);
                        fos.write(new ResizeImageService().resize(Files.readAllBytes(fileInput.toPath()), width--, height--));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else{
            file=fileInput;
        }

        List<String> plates = newArrayList();
//                            plates.add("hj375p");
//                            plates.add("h490bn");
//                            plates.add("42tkb6");
//                            plates.add("88xbbk");

        try {
            HttpResponse<JsonNode> response = Unirest.post("http://api.platerecognizer.com/v1/plate-reader/").header("Authorization", "Token " + token).field("upload", file).asJson();
//                                            System.out.println("Recognize:");
//                                            System.out.println(response.getBody().toString());

            JSONObject node = response.getBody().getObject();

            JSONArray array = node.getJSONArray("results");

            for (int i = 0; i < array.length(); i++) {
                JSONObject result = (JSONObject) array.get(i);

                JSONObject region = result.getJSONObject("region");
                String regionCode = region.getString("code");

                if ("nl".equalsIgnoreCase(regionCode)) {
                    plates.add(result.getString("plate"));
                }
            }


        } catch (Exception e) {
            if (!recursive) {
                recursive = true;
                rateLimiter.acquire();
                return voeruit(bestand, rateLimiter);
            }
            //                            System.out.println(e);
            //                            e.printStackTrace();
        }
//
//
//                                    System.out.println(plates);
        bestand.setKentekens(plates);
        return bestand;
    }
    private double getFileSizeMegaBytes(File file) {
        return (double) file.length() / (1024 * 1024);
    }

    public int getCallsNogOver(){

        HttpResponse<JsonNode> response = Unirest.get("http://api.platerecognizer.com/v1/statistics/").header("Authorization", "Token " + token).asJson();
                                                    System.out.println("Recognize:");
                                                    System.out.println(response.getBody().toString());

        JSONObject node = response.getBody().getObject();


        JSONObject usage = node.getJSONObject("usage");
        String totalCalls = node.getString("total_calls");

        String calls = usage.getString("calls");

return  Integer.valueOf(totalCalls) - Integer.valueOf(calls);
    }
}
