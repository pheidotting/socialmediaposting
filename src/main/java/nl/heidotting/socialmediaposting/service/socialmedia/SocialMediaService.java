package nl.heidotting.socialmediaposting.service.socialmedia;

import nl.heidotting.socialmediaposting.domain.IngeplandePost;
import nl.heidotting.socialmediaposting.domain.Media;
import nl.heidotting.socialmediaposting.repository.IngeplandePostRepository;
import nl.heidotting.socialmediaposting.service.StackStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;

public abstract class SocialMediaService {
    private final static Logger LOGGER = LoggerFactory.getLogger(SocialMediaService.class);
    @Inject
    protected StackStorageService stackStorageService;
    @Inject
    private IngeplandePostRepository ingeplandePostRepository;

    protected InputStream getStackBestand(IngeplandePost geplandePost) {
        return stackStorageService.getIngeplandBestand(geplandePost.getMedia().name() + "-" + geplandePost.getBestandsnaam() + ".jpg");
    }

    public void uitvoeren(IngeplandePost geplandePost) throws IOException{
        String tekst = geplandePost.getTekst().trim();
        if(tekst.startsWith("\n")){
            tekst = tekst.replace("\n","").trim();
        }
        geplandePost.setTekst(tekst);
        voeruit(geplandePost);
        stackStorageService.verwijder(geplandePost.getBestandsnaam(),geplandePost.getMedia());
        ingeplandePostRepository.verwijder(geplandePost.getId());
    }

  public  abstract void voeruit(IngeplandePost geplandePost) throws IOException;

  public  abstract boolean voorMij(Media media);
}
