package nl.heidotting.socialmediaposting.service.socialmedia;

import nl.heidotting.socialmediaposting.domain.IngeplandePost;
import nl.heidotting.socialmediaposting.domain.Media;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.InstagramUploadPhotoRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;

@Service
public class InstagramService extends SocialMediaService {
    private final static Logger LOGGER = LoggerFactory.getLogger(InstagramService.class);

//    @Value("instagram_username")
    private String instagram_username="klveneryoungtimervrienden";
//    @Value("instagram_password")
    private String instagram_password="klazienaveen";

    @Override
    public void voeruit(IngeplandePost geplandePost) throws IOException {
        LOGGER.debug("Uitvoeren {}",geplandePost);
        InputStream stackBestand = getStackBestand(geplandePost);

        Instagram4j instagram = Instagram4j.builder().username(instagram_username).password(instagram_password).build();
        instagram.setup();
        instagram.login();

        File targetFile = File.createTempFile("instagram", "post");

        OutputStream outputStream = new FileOutputStream(targetFile);

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = stackBestand.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }

        targetFile.deleteOnExit();

        instagram.sendRequest(new InstagramUploadPhotoRequest(targetFile, geplandePost.getTekst()));
    }

    @Override
    public boolean voorMij(Media media) {
        return media == Media.INSTAGRAM;
    }
}
