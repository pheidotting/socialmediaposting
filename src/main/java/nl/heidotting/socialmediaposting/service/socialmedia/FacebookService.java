package nl.heidotting.socialmediaposting.service.socialmedia;

import com.restfb.*;
import com.restfb.types.GraphResponse;
import nl.heidotting.socialmediaposting.domain.IngeplandePost;
import nl.heidotting.socialmediaposting.domain.Media;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class FacebookService extends SocialMediaService {
    private final static Logger LOGGER = LoggerFactory.getLogger(FacebookService.class);

    //    @Value("facebook_accessCode")
    private String facebook_accessCode="EAACTjLkmFvYBAEZBZCbvmuRsfWbOKWotR9kNRZCXdp4CYXxRB3ZAk6Y93smYojHRw8BZB5zxyl5b1YPfySLYMY1CFZASZBpZC8moZCPOw567Dn4hxa6hPtfiJgMJ7bFbRg7J9UiAiZAx3k1ZAkA5szZAaqqUA4LWMu5oKWtdmwZBbu0bZAYgZDZD";

    private FacebookClient fbClient;
    private String albumid;

    @Override
    public void voeruit(IngeplandePost geplandePost) throws IOException {
        fbClient = new DefaultFacebookClient(facebook_accessCode, Version.VERSION_7_0);

        InputStream stackBestand = getStackBestand(geplandePost);

        fbClient.publish("me/photos", GraphResponse.class, BinaryAttachment.with("Klazienavener Youngtimer Vrienden.jpg", getBytesFromInputStream(stackBestand), "image/jpeg"), Parameter.with("message", geplandePost.getTekst()));
    }

    @Override
    public boolean voorMij(Media media) {
        return media == Media.FACEBOOK;
    }

    protected byte[] getBytesFromInputStream(InputStream is) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[65535];
            for (int len; (len = is.read(buffer)) != -1; ) {
                os.write(buffer, 0, len);
            }
            os.flush();
            return os.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    //    public void maakAlbum(Bestand bestand) {
    //        String naam = "Klazienavener Youngtimer Vrienden, Virtuele Oldtimerdag 2020";
    //        String omschrijving = "Helaas, in verband met Corona kon vandaag de 2020 editie van de Oldtimerdag Klazienaveen niet doorgaan. We wilden deze dag toch niet stilletjes aan ons voorbij laten gaan, vandaar dat we een oproep deden om foto's te sturen van jouw oldtimer, zodat we deze kunnen bundelen in een fotoalbum zoals we ook gedaan zouden hebben mocht de oldtimerdag toch doorgegaan zijn. Jullie gavan daar gehoor aan, zie hier de door jullie ingestuurde foto's. Geniet er van en hopgelijk tot volgend jaar!\n\n#staystrong #stayhome #wecandothistogether #coronacrisis #flattenthecurve";
    //
    //        Facebook facebook = new FacebookTemplate(accessCode);
    //        MediaOperations mediaOperations = facebook.mediaOperations();
    //
    //        if (albumid == null) {
    //            albumid = mediaOperations.createAlbum(naam, omschrijving);
    //        }
    //
    //        Resource resource = new FileSystemResource(bestand.getFile());
    //
    //        if (bestand.getKentekenMetWaardeMetHashtags().isEmpty()) {
    //            mediaOperations.postPhoto(albumid, resource);
    //        } else {
    //            mediaOperations.postPhoto(albumid, resource, bestand.getTekst());
    //            System.out.println(bestand.getTekst());
    //        }
    //    }
}
