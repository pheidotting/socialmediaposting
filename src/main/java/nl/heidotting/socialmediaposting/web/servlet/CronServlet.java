package nl.heidotting.socialmediaposting.web.servlet;

import nl.heidotting.socialmediaposting.service.InplannenSocialMediaBerichtenService;
import nl.heidotting.socialmediaposting.service.PlaatsMediaOpSocialMediaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class CronServlet {
    private final static Logger LOGGER = LoggerFactory.getLogger(CronServlet.class);

    @Inject
    private PlaatsMediaOpSocialMediaService plaatsMediaOpSocialMediaService;
    @Inject
    private InplannenSocialMediaBerichtenService inplannenSocialMediaBerichtenService;

    @Scheduled(fixedDelay = 174000)
    public void plaatsenMediaOpSocialMeda() {
        LOGGER.info("Eventueel plaatsen van media");
        plaatsMediaOpSocialMediaService.plaatsen();
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void inlenzenNieuweBestanden() {
        LOGGER.info("Inlezen nieuwe bestanden");
        inplannenSocialMediaBerichtenService.startInplannen();
    }

}
