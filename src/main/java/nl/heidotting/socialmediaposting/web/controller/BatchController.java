package nl.heidotting.socialmediaposting.web.controller;

import nl.heidotting.socialmediaposting.service.InplannenSocialMediaBerichtenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
public class BatchController {
    private final static Logger LOGGER = LoggerFactory.getLogger(BatchController.class);

    @Inject
    private InplannenSocialMediaBerichtenService inplannenSocialMediaBerichtenService;

    @RequestMapping("/inplannenSocialMediaBerichten")
    public int inplannenSocialMediaBerichten() {
        LOGGER.info("Start inplannen Berichten");

        return inplannenSocialMediaBerichtenService.startInplannen();
    }

    @RequestMapping("/inplannenSocialMediaBerichten/{force}")
    public int inplannenSocialMediaBerichten(@PathVariable("force") boolean force) {
        LOGGER.info("Start inplannen Berichten");

        return inplannenSocialMediaBerichtenService.startInplannen(force);
    }
}
