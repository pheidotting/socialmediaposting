package nl.heidotting.socialmediaposting.service;

import java.util.List;
import nl.heidotting.socialmediaposting.domain.Bestand;
import nl.heidotting.socialmediaposting.repository.IngeplandePostRepository;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class InplannenServiceTest {
    @TestSubject
    private InplannenService inplannenService=new InplannenService();

    @Mock
    private IngeplandePostRepository ingeplandePostRepository;

    @Test
    void inplannen() {
        List<Bestand > bestanden = new ArrayList<>();

        inplannenService.inplannen(bestanden);
    }
}