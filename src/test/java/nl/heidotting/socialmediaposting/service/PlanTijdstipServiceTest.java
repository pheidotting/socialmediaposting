package nl.heidotting.socialmediaposting.service;

import nl.heidotting.socialmediaposting.domain.IngeplandePost;
import nl.heidotting.socialmediaposting.repository.IngeplandePostRepository;
import nl.heidotting.socialmediaposting.service.testing.EasyMockExtension;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static org.easymock.EasyMock.expect;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
@SpringBootTest
@ExtendWith(EasyMockExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PlanTijdstipServiceTest extends EasyMockSupport {
    @TestSubject
    private PlanTijdstipService planTijdstipService = new PlanTijdstipService();

    @Mock
    private IngeplandePostRepository ingeplandePostRepository;

    private List<IngeplandePost> ingeplandePosts = new ArrayList<>();

    @RepeatedTest(value = 1, name = RepeatedTest.LONG_DISPLAY_NAME)
    @DisplayName("Geen posts in de toekomst gepland, 1 nieuw bestand inplannen")
    public void testKomMetTijdstip() {
        expect(ingeplandePostRepository.leesIngeplandePostsInDeToekomst()).andReturn(ingeplandePosts);

        replayAll();

        LocalDateTime tijdstip = planTijdstipService.komMetTijdstip();

        verifyAll();

        assertNotNull(tijdstip);

        LocalDateTime nu = LocalDateTime.now();
        LocalDateTime overEenJaar = LocalDateTime.now().plusYears(1);

        assertTrue(tijdstip.isBefore(overEenJaar));
        assertTrue(tijdstip.isAfter(nu));
    }

    @RepeatedTest(value = 500, name = RepeatedTest.LONG_DISPLAY_NAME)
    @DisplayName("Geen posts in de toekomst gepland, {totalRepetitions} nieuw bestanden inplannen")
    void testKomMetTijdstipMulti(TestReporter testReporter, RepetitionInfo repetitionInfo) {
        expect(ingeplandePostRepository.leesIngeplandePostsInDeToekomst()).andReturn(ingeplandePosts).anyTimes();

        replayAll();

        LocalDateTime tijdstip = planTijdstipService.komMetTijdstip();
        ingeplandePosts.add(new IngeplandePost(tijdstip, null, null, null));

        //        System.out.println(tijdstip.toString());

        verifyAll();

        resetAll();

        assertNotNull(tijdstip);

        LocalDateTime nu = LocalDateTime.now();

        assertTrue(tijdstip.isAfter(nu), tijdstip + " is voor nu (" + nu + ")");

        ingeplandePosts.stream().map(ingeplandePost12 -> ingeplandePost12.getTijdstipIngepland()).forEach(localDateTime -> {
            List<LocalDateTime> meerDanEen = ingeplandePosts.stream()//
                    .map(ingeplandePost1 -> ingeplandePost1.getTijdstipIngepland())//
                    .filter(localDateTime1 -> localDateTime.equals(localDateTime1))//
                    .collect(Collectors.toList());

            if (meerDanEen.size() > 1) {
                System.out.println(meerDanEen);
                throw new AssertionError("Meer dan 1 " + localDateTime + " gevonden.");
            }
        });
    }

    @AfterAll
    void print() {
        Map<LocalDate, List<LocalDateTime>> map = new HashMap<>();
        ingeplandePosts.stream().forEach(new Consumer<IngeplandePost>() {
            @Override
            public void accept(IngeplandePost ingeplandePost) {
                List<LocalDateTime> ldt = map.get(ingeplandePost.getTijdstipIngepland().toLocalDate());
                if (ldt == null) {
                    ldt = new ArrayList<>();
                }
                ldt.add(ingeplandePost.getTijdstipIngepland());

                map.put(ingeplandePost.getTijdstipIngepland().toLocalDate(), ldt);
            }
        });
        List<LocalDate> localDates = map.keySet().stream().collect(Collectors.toList());
        Collections.sort(localDates, Comparator.naturalOrder());
        localDates.stream().forEach(new Consumer<LocalDate>() {
            @Override
            public void accept(LocalDate localDate) {

                PlanTijdstipService.Dag dag = PlanTijdstipService.Dag.getFromDayOfWeek(localDate.getDayOfWeek());
                List<LocalDateTime> lijst = map.get(localDate);
                Collections.sort(lijst, Comparator.naturalOrder());

                System.out.println(localDate + " - " + lijst.size() + " - " + dag.getMinimumAantalPosts() + "-" + dag.getMaximumAantalPosts() + " - " + lijst);
            }
        });
    }

}